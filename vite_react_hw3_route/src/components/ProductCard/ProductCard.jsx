import React from "react";
import PropTypes from "prop-types";
import "./ProductCard.scss";
import Button from "../Button/Button.jsx";
import Zoom from "../../assets/icons/zoom.svg?react";
import Star from "../../assets/icons/star.svg?react";
import Basket from "../../assets/icons/basket.svg?react";
import Close from "../../assets/icons/close_X.svg?react";

const ProductCard = ({ product, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent, handleModalDeleteFromBasket, handleRemoveFromBasket, isBasketPage }) => {
    const { nameCard, price, imgCard, article, color, isFavorited, countInBasket } = product;

    return (
        <div className="product-card">
            <div className="product-card_icons">
                <Button onClick={() => {
                    handleImgZoomModal();
                    setModalContent({ imgCard: imgCard, nameCard: nameCard, })
                }}>< Zoom />
                </Button>
                <Button
                    className={isFavorited === true ? "isFavorited" : ""}
                    onClick={() => {
                        handleFavoritedList(product);
                    }}>
                    < Star />
                </Button>
                {/* Кнопка для додавання в кошик або видалення з кошика */}
                {isBasketPage ? (
                    // Якщо на сторінці кошика, відображаємо кнопку видалення
                    <Button onClick={() => {
                        // handleRemoveFromBasket(product); // Викликати функцію видалення по хрестику
                        handleModalDeleteFromBasket(product); // Викликати функцію видалення через модалку
                        // console.log("product for delete", product)
                    }}>
                        < Close />
                    </Button>
                ) : (
                    // Якщо НЕ на сторінці кошика, відображаємо кнопку додавання в кошик
                    <Button
                        onClick={() => {
                            handleAddBasketModal();
                            setModalContent({
                                imgCard: imgCard,
                                modalText: "Add to basket",
                                nameCard: nameCard,
                                price: price,
                                article: article,
                                color: color,
                                isFavorited: isFavorited,
                                countInBasket: countInBasket
                            })
                        }}
                    >
                        < Basket />
                    </Button>
                )}
            </div>
            <img src={imgCard} alt={nameCard} className="product-card_img" />
            <h2 className="product-card_title">{nameCard}</h2>
            <p><span className="product-card_description">Price: </span>{price}$</p>
            <p><span className="product-card_description">Article: </span>{article}</p>
            <p><span className="product-card_description">Color: </span>{color}</p>
            <p><span className="product-card_description">countInBasket: </span>{countInBasket}</p>
        </div>
    );
};

ProductCard.propTypes = {
    product: PropTypes.object,
    handleImgZoomModal: PropTypes.func,
    handleFavoritedList: PropTypes.func,
    handleAddBasketModal: PropTypes.func,
    setModalContent: PropTypes.func,
    handleRemoveFromBasket: PropTypes.func,
    handleModalDeleteFromBasket: PropTypes.func,
    isBasketPage: PropTypes.bool,
    nameCard: PropTypes.string,
    price: PropTypes.number,
    imgCard: PropTypes.any,
    article: PropTypes.string,
    color: PropTypes.string,
    isFavorited: PropTypes.bool,
    countInBasket: PropTypes.number
};

export default ProductCard;
