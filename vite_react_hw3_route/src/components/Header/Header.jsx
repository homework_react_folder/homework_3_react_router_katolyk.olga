import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import PropTypes from "prop-types";
import Star from "../../assets/icons/star.svg?react";
import Basket from "../../assets/icons/basket.svg?react";
import Logo from "../Logo/logo.svg?react";
import "./Header.scss";

const Header = ({ favoritedList, basketList }) => {
  // Підрахунок загальної кількості товарів у кошику
  const totalBasketCount = basketList.reduce((total, item) => total + item.countInBasket, 0);
  
  return (
    <header className="header">
      <Link to="/" className="button">< Logo /></Link>
      <h1 className="header_title">My toy-shop</h1>
      <div className="header_icons">
        <div className="header_star">
          <NavLink to="/favoritepage" className="button">< Star /></NavLink>
          <p className="header-icons_count">{favoritedList.length}</p>
        </div>
        <div className="header_basket">
          <NavLink to="/basketpage" className="button">< Basket /></NavLink>
          <p className="header-icons_count">{totalBasketCount}</p>
        </div>
      </div>
    </header>
  )
};

Header.propTypes = {
  favoritedList: PropTypes.array,
  basketList: PropTypes.array
};

export default Header;
