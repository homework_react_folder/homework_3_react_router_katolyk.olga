import React from "react";
import './BasketPage.scss';
import PropTypes from "prop-types";
import ProductList from "../../components/ProductList/ProductList";

const BasketPage = ({ product, basketList, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent, handleModalDeleteFromBasket, handleRemoveFromBasket, isBasketPage }) => {
    return (
        <>
            <div className="page__title">
                <h2 className="page__title-text">Product in the basket:</h2>
            </div>
            <ProductList
                products={basketList}
                handleImgZoomModal={handleImgZoomModal}
                handleFavoritedList={handleFavoritedList}
                handleAddBasketModal={handleAddBasketModal}
                setModalContent={setModalContent} 
                handleModalDeleteFromBasket={handleModalDeleteFromBasket} 
                handleRemoveFromBasket={handleRemoveFromBasket}
                isBasketPage={isBasketPage}
                product={product}
            />
        </>
    )
}

BasketPage.propTypes = {
    basketList: PropTypes.array,
    handleImgZoomModal: PropTypes.func,
    handleFavoritedList: PropTypes.func,
    handleAddBasketModal: PropTypes.func,
    setModalContent: PropTypes.func,
    handleRemoveFromBasket: PropTypes.func,
    isBasketPage: PropTypes.bool
};

export default BasketPage;
