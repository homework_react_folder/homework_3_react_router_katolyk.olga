import React from "react";
import './HomePage.scss';
import ProductList from "../../components/ProductList/ProductList";

const HomePage = ({ products, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent }) => {
    return (
        <ProductList
          products={products}
          handleImgZoomModal={handleImgZoomModal}
          handleFavoritedList={handleFavoritedList}
          handleAddBasketModal={handleAddBasketModal}
          setModalContent={setModalContent}
        />
    )
}

export default HomePage;
