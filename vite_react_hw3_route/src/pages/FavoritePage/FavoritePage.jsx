import React from "react";
import './FavoritePage.scss';
import PropTypes from "prop-types";
import ProductList from "../../components/ProductList/ProductList";

const FavoritePage = ({ favoritedList, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent }) => {
  return (
    <>
      <div className="page__title">
        <h2 className="page__title-text">List of favorited products:</h2>
      </div>
      <ProductList
        products={favoritedList}
        handleImgZoomModal={handleImgZoomModal}
        handleFavoritedList={handleFavoritedList}
        handleAddBasketModal={handleAddBasketModal}
        setModalContent={setModalContent}
      />
    </>
  )
}

FavoritePage.propTypes = {
  favoritedList: PropTypes.array,
  handleImgZoomModal: PropTypes.func,
  handleFavoritedList: PropTypes.func,
  handleAddBasketModal: PropTypes.func,
  setModalContent: PropTypes.func
};

export default FavoritePage;
