Завдання
Хуки та роутинг.

Для виконання цього завдання необхідно доповнити проект, створений у попередньому домашньому завданні homework2.

Технічні вимоги:
+ Додати в програму дві сторінки - Кошик та Вибране.
+ На цих сторінках для виведення інформації можна використовувати ту саму картку товару, яка використовується на головній сторінці.
+ Користувач повинен мати можливість видалити товар з кошика. Для цього поруч із карткою товару має бути іконка з хрестиком. 
- При видаленні з кошика - перед самим видаленням повинне бути підтвердження дії - модальне вікно з домашнього завдання homework1.
+ Користувач повинен мати можливість видалити товар з обраного. Видалення з обраного відбувається натисканням на іконку зірочки.
+ У верхньому меню мають бути посилання на всі три сторінки програми.
+ Перехід між сторінками повинен здійснюватися без перезавантаження сторінки за допомогою бібліотеки react-router-dom.





<!-- 
Для вирішення цієї проблеми вам потрібно оновлювати значення isFavorited у відповідному об'єкті товару після додавання або видалення його з обраного на сторінці кошика.

Ось як ви можете це зробити:

Додайте функцію updateIsFavoritedInMainList, яка буде оновлювати значення isFavorited у головному списку товарів після зміни стану обраного товару.
javascript
Copy code
const updateIsFavoritedInMainList = (article, isFavorited) => {
    setProducts(prevProducts => {
        return prevProducts.map(product => {
            if (product.article === article) {
                return { ...product, isFavorited };
            } else {
                return product;
            }
        });
    });
};
Передайте цю функцію у компоненти, де ви додаєте або видаляєте товари з обраного.
Викличте цю функцію, щоб оновити значення isFavorited у відповідному об'єкті товару після зміни стану обраного товару.
Ось приклад, як це може бути зроблено у вашому компоненті BasketPage:

javascript
Copy code
const BasketPage = ({ basketList, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent, handleRemoveFromBasket, isBasketPage, updateIsFavoritedInMainList }) => {
    return (
        <>
            <div className="page__title">
                <h2 className="page__title-text">Product in the basket:</h2>
            </div>
            <ProductList
                products={basketList}
                handleImgZoomModal={handleImgZoomModal}
                handleFavoritedList={handleFavoritedList}
                handleAddBasketModal={handleAddBasketModal}
                setModalContent={setModalContent}
                handleRemoveFromBasket={handleRemoveFromBasket}
                isBasketPage={isBasketPage}
                updateIsFavoritedInMainList={updateIsFavoritedInMainList} // Передайте функцію у ProductList
            />
        </>
    )
}
А потім у вашому компоненті ProductList, ви можете передати цю функцію у компонент ProductCard:

javascript
Copy code
<ProductCard 
    key={product.article} 
    product={product} 
    handleImgZoomModal={handleImgZoomModal} 
    handleFavoritedList={handleFavoritedList}
    handleAddBasketModal={handleAddBasketModal} 
    setModalContent={setModalContent}
    handleRemoveFromBasket ={handleRemoveFromBasket}
    isBasketPage={isBasketPage}
    updateIsFavoritedInMainList={updateIsFavoritedInMainList} // Передайте функцію у ProductCard
/>
І у вашому компоненті ProductCard, ви можете використати цю функцію після зміни стану обраного товару:

javascript
Copy code
const handleFavoritedList = (product) => {
    // Оновлюємо стан isFavorited у головному списку товарів
    updateIsFavoritedInMainList(product.article, !product.isFavorited);

    // Решта вашого коду
};
Це забезпечить синхронізацію стану обраного товару між сторінкою кошика та сторінкою обраного. -->